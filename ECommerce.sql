
--use ecommercedatabase
--select * from products;  select * from usertable; select * from ordertable;

--VIEWS
--1. List of products under a particular category

create view view_listproductswithcategory as select p.productid,p.productname,p.productprice,p.dateofmanufacture,c.categoryname from products p, category c where p.categoryid =c.categoryid;
select * from view_listproductswithcategory;

--2. List of categories whose products expires with one day


--3. List of products with more sales

--create view moresales1 as select p.productname,p.productdescription,p.productprice from products p, ordertable o where p.productid=o.productid  and o.productid in (select productId from ordertable o1,ordertable o2  group by  productid having count(o1.ProductId)>count(o2.productId))
--select * from moresales;

create view maxcountproduct as select p.productname ,count(o.productid) as maxcount  from products p, ordertable o where p.productid=o.productid group by p.productname 

select * from maxcountproduct

select productname from maxcountproduct where maxcount=(select max(maxcount) from maxcountproduct)

--4. list of products purchased by user within one month

--5. list of users who purchased more than 10 lakhs within a month
create view morethantenlakhs1 as select p.productname,p.productdescription,p.productprice from products p,usertable u where p.userid=u.userid and p.productprice*u.productquantity >1000000 ;

select * from morethantenlakhs1

--6.search a product based on product name

--create view prdouctnameView2 as select * from products where productname='pencil';

--STORED PROCEDURE FOR PRODUCTNAME
create procedure viewprodname @productName varchar(20)
as
Begin
select * from products where productname=@productName
end

exec viewprodname 'pen_parker'

--7. Search a product based on category
create procedure viewproductsoncategory2 @categoryName varchar(20)
as
Begin
select p.ProductName,p.ProductPrice,p.ProductDescription from products p, category c where p.CategoryId=c.CategoryId and c.CategoryName=@categoryname
end

exec viewproductsoncategory2 'pen'


--select * from category
