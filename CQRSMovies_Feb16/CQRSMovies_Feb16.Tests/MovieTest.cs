using CQRSMovies_Feb16.Controllers;
using CQRSMovies_Feb16.DataAccess;
using CQRSMovies_Feb16.Models;
using Moq;
using System;
using System.Collections.Generic;

namespace CQRSMovies_Feb16.Tests
{
    public class MovieTest
    {
        //add mock as handling all the functionalities

        public Mock<IMovies> mock = new Mock<IMovies>();

        [Fact]
        public void Test1()
        {

        }
        [Fact]
        public void GetMovieByType()
        {

            //List<Tmovie> movielist = new List<Tmovie>()
            //{
            //    new Tmovie(){ MovieId = 1, MovieName = "RRR", MovieType = "Thriller" }


            //};


            ////arrange
            //mock.Setup(p => p.getMovieByType("Thriller")).Returns(movielist);
            //MovieController movie = new MovieController(mock.Object);

            ////act
            //var result = movie.getMoviesByType("Thriller");
            ////assert
            //Assert.Equal(movielist, result);

        }
        [Fact]
        public async void GetMovieDetails()
        {
            var moviesDTO = new Tmovie()
            {
                MovieId= 10,
                MovieName="ABCD",
                MovieType="Comedy"

            };
            //arrange

           mock.Setup(p => p.getMovieById(10)).Returns(moviesDTO);
            MovieController mov = new MovieController(mock.Object);

            // //act
            var result = await mov.GetMovieDetailsById(10);
            // //assert
            Assert.True(moviesDTO.Equals(result));


        }
    }
    }
