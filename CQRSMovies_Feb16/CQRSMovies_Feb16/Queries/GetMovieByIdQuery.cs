﻿using CQRSMovies_Feb16.Models;
using MediatR;

namespace CQRSMovies_Feb16.Queries
{
    public record GetMovieByIdQuery(int id) : IRequest<Tmovie>;
    
}
