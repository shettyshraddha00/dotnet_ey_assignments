﻿using System;
using System.Collections.Generic;

namespace CQRSMovies_Feb16.Models
{
    public partial class Tmovie
    {
        public int MovieId { get; set; }
        public string? MovieName { get; set; }
        public string? MovieType { get; set; }
    }
}
