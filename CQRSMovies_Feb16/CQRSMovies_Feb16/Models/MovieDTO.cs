﻿namespace CQRSMovies_Feb16.Models
{
    public class MovieDTO
    {
        public int MovieId { get; set; }
        public string? MovieName { get; set; }
        public string? MovieType { get; set; }
    }
}
