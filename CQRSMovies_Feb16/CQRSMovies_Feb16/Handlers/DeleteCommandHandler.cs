﻿using CQRSMovies_Feb16.Commands;
using CQRSMovies_Feb16.DataAccess;
using MediatR;

namespace CQRSMovies_Feb16.Handlers
{
    public class DeleteCommandHandler: IRequestHandler<DeleteCommand,string>
    {
        private readonly IMovies _movies;

        public DeleteCommandHandler(IMovies movies)
        {
            _movies = movies;
        }

        public Task<string> Handle(DeleteCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_movies.deleteMovie(request.id));
        }
    }

}
