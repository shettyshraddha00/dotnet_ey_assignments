﻿using CQRSMovies_Feb16.DataAccess;
using CQRSMovies_Feb16.Models;
using CQRSMovies_Feb16.Queries;
using MediatR;

namespace CQRSMovies_Feb16.Handlers
{
    public class GetAllMovieHandler :IRequestHandler<GetAllMovies, List<Tmovie>>
    {
        private readonly IMovies _movies;

        public GetAllMovieHandler(IMovies movies)
        {
            _movies= movies;
        }

        public async Task<List<Tmovie>> Handle(GetAllMovies request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_movies.getAllMovies());
        }
    }
}
