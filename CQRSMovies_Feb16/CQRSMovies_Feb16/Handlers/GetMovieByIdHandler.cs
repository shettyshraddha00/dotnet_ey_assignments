﻿using CQRSMovies_Feb16.DataAccess;
using CQRSMovies_Feb16.Models;
using CQRSMovies_Feb16.Queries;
using MediatR;

namespace CQRSMovies_Feb16.Handlers
{
    public class GetMovieByIdHandler :IRequestHandler<GetMovieByIdQuery,Tmovie>
    {
        private readonly IMovies _movies;

        public GetMovieByIdHandler(IMovies movies)
        {
            _movies=movies;
        }

        public Task<Tmovie> Handle(GetMovieByIdQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_movies.getMovieById(request.id));
        }
    }
}
