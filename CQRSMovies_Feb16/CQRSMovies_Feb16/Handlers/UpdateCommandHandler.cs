﻿using CQRSMovies_Feb16.Commands;
using CQRSMovies_Feb16.DataAccess;
using CQRSMovies_Feb16.Models;
using MediatR;

namespace CQRSMovies_Feb16.Handlers
{
    public class UpdateCommandHandler :IRequestHandler<UpdateCommand, List<Tmovie>>
    {
        private readonly IMovies _movies;

        public UpdateCommandHandler(IMovies movies)
        {
            _movies = movies;
        }

        public async Task<List<Tmovie>> Handle(UpdateCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_movies.updateMovie(request.movie));
        }
    }
}
