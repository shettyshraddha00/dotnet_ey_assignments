﻿using CQRSMovies_Feb16.Commands;
using CQRSMovies_Feb16.DataAccess;
using CQRSMovies_Feb16.Models;
using MediatR;

namespace CQRSMovies_Feb16.Handlers
{
    public class AddNewCommandHandler :IRequestHandler<AddNew, List<Tmovie>>
    {
        private readonly IMovies _movies;

        public AddNewCommandHandler(IMovies movies)
        {
            _movies = movies;
        }

        public async Task<List<Tmovie>> Handle(AddNew request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_movies.addNewMovie(request.movie));
        }
    }
}
