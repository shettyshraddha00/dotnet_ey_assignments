﻿using CQRSMovies_Feb16.Commands;
using CQRSMovies_Feb16.DataAccess;
using CQRSMovies_Feb16.Models;
using CQRSMovies_Feb16.Queries;
using MediatR;

namespace CQRSMovies_Feb16.Handlers
{
    public class GetAllMoviesByTypeHandler :IRequestHandler<GetMoviesByType,List<Tmovie>>
    {
        private readonly IMovies _movies;

        public GetAllMoviesByTypeHandler(IMovies movies)
        {
            _movies = movies;
        }

        public Task<List<Tmovie>> Handle(GetMoviesByType request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_movies.getMovieByType(request.movieType));
        }

       
    }
}
