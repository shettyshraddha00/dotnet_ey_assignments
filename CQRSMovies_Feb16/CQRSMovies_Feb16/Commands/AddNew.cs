﻿using CQRSMovies_Feb16.Models;
using CQRSMovies_Feb16.Queries;
using MediatR;

namespace CQRSMovies_Feb16.Commands
{
    public record AddNew(Tmovie movie) : IRequest<List<Tmovie>>;
   
}
