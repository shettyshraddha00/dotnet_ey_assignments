﻿using CQRSMovies_Feb16.Models;
using MediatR;

namespace CQRSMovies_Feb16.Commands
{
    public record UpdateCommand(Tmovie movie):IRequest<List<Tmovie>>;
    
}
