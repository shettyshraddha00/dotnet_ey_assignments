﻿using MediatR;

namespace CQRSMovies_Feb16.Commands
{
    public record DeleteCommand(int id) :IRequest<string>;
   
}
