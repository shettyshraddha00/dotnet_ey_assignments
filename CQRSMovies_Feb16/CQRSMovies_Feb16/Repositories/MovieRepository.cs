﻿using CQRSMovies_Feb16.DataAccess;
using CQRSMovies_Feb16.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace CQRSMovies_Feb16.Repositories
{
    public class MovieRepository : IMovies
    {

        private readonly MoviesContext _context;
        public MovieRepository(MoviesContext context)
        {
            _context = context;
        }

        //add new movies

        public List<Tmovie> addNewMovie(Tmovie movie)
        {
            _context.Tmovies.Add(movie);
            _context.SaveChanges();
            return _context.Tmovies.ToList();
        }

        //delete movie
        public string deleteMovie(int id)
        {
            var getMovieById = _context.Tmovies.Find(id);
            try
            {
                if (getMovieById!= null)
                {
                    _context.Tmovies.Remove(getMovieById);
                    _context.SaveChanges();
                   
                }
            }  
            catch (Exception e) 
            {
                throw new InvalidDataException("Not deleted");
            }
            return "Deleted";
        }

        //get all movies
        public List<Tmovie> getAllMovies()
        {
           return _context.Tmovies.ToList();
        }

        public Tmovie getMovieById(int id)
        {
            Tmovie movieDetails = _context.Tmovies.Find(id);

            return movieDetails;
            
        }

        //public async List<Tmovie> getMovieById(int id)
        //{
        //    var getID = await _context.Tmovies.Find(id);
        //    return 
        //}

        //public string MovieByName(int id)
        //{
        //    var getMovieName =  _context.Tmovies.Where(x => x.MovieId==id).Select(x => x.MovieName).FirstOrDefault();
        //    return getMovieName;
        //}

        //by movie type
        public List<Tmovie> getMovieByType(string movieType)
        {
            var listMovies = _context.Tmovies.Where(x => x.MovieType == movieType).ToList();
            return listMovies;
            //var getMovieByType= _context.Tmovies.Find(movieType);    
           // return _context.Tmovies.ToList();      
        }

        //update  movie
        public List<Tmovie> updateMovie(Tmovie movie)
        {
            var getMovieById = _context.Tmovies.Find(movie.MovieId);
           
            if (getMovieById != null)
            {
                getMovieById.MovieName= movie.MovieName;
                getMovieById.MovieType= movie.MovieType;
            }
            _context.SaveChanges();
            return _context.Tmovies.ToList();
        }

       
    }
       
    }

