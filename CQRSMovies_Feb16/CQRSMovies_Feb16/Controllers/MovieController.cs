﻿using CQRSMovies_Feb16.Commands;
using CQRSMovies_Feb16.DataAccess;
using CQRSMovies_Feb16.Models;
using CQRSMovies_Feb16.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CQRSMovies_Feb16.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly IMediator _mediator;

        private readonly IMovies _movie;

        public MovieController(IMovies movies)
        {
           _movie = movies;
        }

        public MovieController(IMediator mediator)
        {
            _mediator = mediator;
        }

        private readonly IMovies _movies;

        //public MovieController(IMovies movies)
        //{
        //    _movies = movies;
        //}

        [HttpGet]
        public async Task<IActionResult> GetMovies()
        {
            var movies = await _mediator.Send(new GetAllMovies());
            return Ok(movies);
        }

        [HttpPost]
        public async Task<ActionResult> CreateNew([FromBody] Tmovie movie)
        {
            await _mediator.Send(new AddNew(movie));
            return StatusCode(201);
        }
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] Tmovie movie)
        {
            await _mediator.Send(new UpdateCommand(movie));
            return StatusCode(201);
        }
        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            await _mediator.Send(new DeleteCommand(id));
            return StatusCode(200);
        }
        // [HttpGet("{movieType}")]
        [HttpGet]
        [Route("{movieType}")]
        public async Task<ActionResult> getMoviesByType(string movieType)
        {
            var movies=await _mediator.Send(new GetMoviesByType(movieType));
            return Ok(movies);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult> GetMovieDetailsById(int id)
        {
            var movies = await _mediator.Send(new GetMovieByIdQuery(id));
            return Ok(movies);
        }

    }
}
