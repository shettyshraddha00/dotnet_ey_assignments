﻿using CQRSMovies_Feb16.Models;

namespace CQRSMovies_Feb16.DataAccess
{
    public interface IMovies
    {
        List<Tmovie> getAllMovies();

        List<Tmovie> addNewMovie(Tmovie movie);

        List<Tmovie> getMovieByType(string movieType);

        List<Tmovie> updateMovie(Tmovie movie);

        string deleteMovie(int id);

       Tmovie getMovieById(int id);

       // string getMovieByName(int id);
       
    }
}
