﻿using System;
using System.Collections.Generic;

namespace CQRSMovieGenreTwoTables_Feb16.Models;

public partial class Tgenre
{
    public int GenreId { get; set; }

    public string? GenreType { get; set; }
}
