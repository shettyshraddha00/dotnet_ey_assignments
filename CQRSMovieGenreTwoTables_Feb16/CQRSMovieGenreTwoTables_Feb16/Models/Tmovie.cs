﻿using System;
using System.Collections.Generic;

namespace CQRSMovieGenreTwoTables_Feb16.Models;

public partial class Tmovie
{
    public int MovieId { get; set; }

    public string? MovieName { get; set; }

    public string? MovieActor { get; set; }

    public string? MovieActress { get; set; }

    public int? GenreId { get; set; }
}
