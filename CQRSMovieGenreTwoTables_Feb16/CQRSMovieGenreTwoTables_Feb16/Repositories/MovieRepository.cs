﻿using CQRSMovieGenreTwoTables_Feb16.DataAccess;
using CQRSMovieGenreTwoTables_Feb16.Models;

namespace CQRSMovieGenreTwoTables_Feb16.Repositories
{
    public class MovieRepository : IMovie
    {
        private readonly MovieGenreContext _context;
        public MovieRepository(MovieGenreContext context)
        {
            _context = context;
        }

        //add new movies

        public List<Tmovie> addNewMovie(Tmovie movie)
        {
            _context.Tmovies.Add(movie);
            _context.SaveChanges();
            return _context.Tmovies.ToList();
        }

        //delete movie
        public string deleteMovie(int id)
        {
            var getMovieById = _context.Tmovies.Find(id);
            try
            {
                if (getMovieById!= null)
                {
                    _context.Tmovies.Remove(getMovieById);
                    _context.SaveChanges();

                }
            }
            catch (Exception e)
            {
                throw new InvalidDataException("Not deleted");
            }
            return "Deleted";
        }

        //get all movies
        public List<Tmovie> getAllMovies()
        {
            return _context.Tmovies.ToList();
        }

        public List<Tmovie> getByGenre(int genreId)
        {

            var getGenreById = _context.Tmovies.Find(genreId);
            List<Tmovie> movieByGenre = _context.Tmovies.Where(x => x.GenreId==genreId).ToList();
            return movieByGenre;

        }



        //update  movie
        public List<Tmovie> updateMovie(Tmovie movie)
        {
            var getMovieById = _context.Tmovies.Find(movie.MovieId);

            if (getMovieById != null)
            {
                getMovieById.MovieName= movie.MovieName;
                getMovieById.MovieActor= movie.MovieActor;
                getMovieById.MovieActress= movie.MovieActress;
            }
            _context.SaveChanges();
            return _context.Tmovies.ToList();
        }

    }
}
