﻿using CQRSMovieGenreTwoTables_Feb16.Models;

namespace CQRSMovieGenreTwoTables_Feb16.DataAccess
{
    public interface IMovie
    {
        List<Tmovie> getAllMovies();

        List<Tmovie> addNewMovie(Tmovie movie);

        List<Tmovie> updateMovie(Tmovie movie);

        string deleteMovie(int id);

        List<Tmovie> getByGenre(int genreId);
    }
}
