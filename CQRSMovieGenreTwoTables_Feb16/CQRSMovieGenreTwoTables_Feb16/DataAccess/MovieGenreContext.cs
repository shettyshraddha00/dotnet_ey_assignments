﻿using System;
using System.Collections.Generic;
using CQRSMovieGenreTwoTables_Feb16.Models;
using Microsoft.EntityFrameworkCore;

namespace CQRSMovieGenreTwoTables_Feb16.DataAccess;

public partial class MovieGenreContext : DbContext
{
    public MovieGenreContext()
    {
    }

    public MovieGenreContext(DbContextOptions<MovieGenreContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Tgenre> Tgenres { get; set; }

    public virtual DbSet<Tmovie> Tmovies { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }
    //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
    //        => optionsBuilder.UseSqlServer("server=localhost;trusted_connection=true;database=MovieGenre;TrustServerCertificate=True;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Tgenre>(entity =>
        {
            entity.HasKey(e => e.GenreId);

            entity.ToTable("TGenre");

            entity.Property(e => e.GenreType)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Tmovie>(entity =>
        {
            entity.HasKey(e => e.MovieId);

            entity.ToTable("TMovie");

            entity.Property(e => e.MovieActor)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.MovieActress)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.MovieName)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
