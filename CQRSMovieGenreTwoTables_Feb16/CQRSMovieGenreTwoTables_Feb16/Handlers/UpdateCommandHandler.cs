﻿using CQRSMovieGenreTwoTables_Feb16.Commands;
using CQRSMovieGenreTwoTables_Feb16.DataAccess;
using CQRSMovieGenreTwoTables_Feb16.Models;
using MediatR;

namespace CQRSMovieGenreTwoTables_Feb16.Handlers
{
    public class UpdateCommandHandler : IRequestHandler<UpdateCommand, List<Tmovie>>
    {
        private readonly IMovie _movies;

        public UpdateCommandHandler(IMovie movies)
        {
            _movies = movies;
        }

        public async Task<List<Tmovie>> Handle(UpdateCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_movies.updateMovie(request.movie));
        }
    }
}
