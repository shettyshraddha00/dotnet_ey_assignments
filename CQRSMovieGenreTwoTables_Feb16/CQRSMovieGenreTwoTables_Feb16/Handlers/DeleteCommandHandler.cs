﻿using CQRSMovieGenreTwoTables_Feb16.Commands;
using CQRSMovieGenreTwoTables_Feb16.DataAccess;
using MediatR;

namespace CQRSMovieGenreTwoTables_Feb16.Handlers
{
    public class DeleteCommandHandler : IRequestHandler<DeleteCommand, string>
    {
        private readonly IMovie _movies;

        public DeleteCommandHandler(IMovie movies)
        {
            _movies = movies;
        }

        public Task<string> Handle(DeleteCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_movies.deleteMovie(request.id));
        }
    }
}
