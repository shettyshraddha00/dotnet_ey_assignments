﻿using CQRSMovieGenreTwoTables_Feb16.DataAccess;
using CQRSMovieGenreTwoTables_Feb16.Models;
using CQRSMovieGenreTwoTables_Feb16.Queries;
using MediatR;
using Microsoft.AspNetCore.Authentication;

namespace CQRSMovieGenreTwoTables_Feb16.Handlers
{
    public class GetMovieQueryHandler : IRequestHandler<GetMoviesQuery, List<Tmovie>>
    {
        private readonly IMovie _movie;

        public GetMovieQueryHandler(IMovie movie)
        {
            _movie= movie;
        }
        public Task<List<Tmovie>> Handle(GetMoviesQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_movie.getAllMovies());
        }
    }
}
