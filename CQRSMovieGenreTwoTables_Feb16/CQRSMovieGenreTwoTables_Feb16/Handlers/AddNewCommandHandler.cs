﻿using CQRSMovieGenreTwoTables_Feb16.Commands;
using CQRSMovieGenreTwoTables_Feb16.DataAccess;
using CQRSMovieGenreTwoTables_Feb16.Models;
using MediatR;

namespace CQRSMovieGenreTwoTables_Feb16.Handlers
{
    public class AddNewCommandHandler : IRequestHandler<AddNew, List<Tmovie>>
    {
        private readonly IMovie _movies;

        public AddNewCommandHandler(IMovie movies)
        {
            _movies = movies;
        }

        public async Task<List<Tmovie>> Handle(AddNew request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_movies.addNewMovie(request.movie));
        }
    }
}
