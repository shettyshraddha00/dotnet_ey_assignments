﻿using CQRSMovieGenreTwoTables_Feb16.DataAccess;
using CQRSMovieGenreTwoTables_Feb16.Models;
using CQRSMovieGenreTwoTables_Feb16.Queries;
using MediatR;

namespace CQRSMovieGenreTwoTables_Feb16.Handlers
{
    public class GetMovieByGenreIdHandler : IRequestHandler<GetMovieByGenre,List<Tmovie>>
    {
        private readonly IMovie _movie;
        public GetMovieByGenreIdHandler(IMovie movie ) { 
            _movie = movie;
        }

        public async Task<List<Tmovie>> Handle(GetMovieByGenre request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_movie.getByGenre(request.genreId));
        }
    }
}
