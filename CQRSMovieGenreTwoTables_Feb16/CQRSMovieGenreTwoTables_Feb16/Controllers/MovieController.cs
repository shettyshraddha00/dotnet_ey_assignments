﻿using CQRSMovieGenreTwoTables_Feb16.Commands;
using CQRSMovieGenreTwoTables_Feb16.Models;
using CQRSMovieGenreTwoTables_Feb16.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CQRSMovieGenreTwoTables_Feb16.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MovieController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpGet]
        public async Task<IActionResult> GetMovies()
        {
            var movies = await _mediator.Send(new GetMoviesQuery());
            return Ok(movies);
        }

        [HttpPost]
        public async Task<ActionResult> CreateNew([FromBody] Tmovie movie)
        {
            await _mediator.Send(new AddNew(movie));
            return StatusCode(201);
        }
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] Tmovie movie)
        {
            await _mediator.Send(new UpdateCommand(movie));
            return StatusCode(201);
        }
        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            await _mediator.Send(new DeleteCommand(id));
            return StatusCode(200);
        }

        [HttpGet("{genreId}")]
        public async Task<IActionResult> getMoviesById(int genreId)
        {
            var movies =await _mediator.Send(new GetMovieByGenre(genreId));
            return Ok(movies);
        }
        
    }
}
