﻿using CQRSMovieGenreTwoTables_Feb16.Models;
using MediatR;

namespace CQRSMovieGenreTwoTables_Feb16.Commands
{
    public record UpdateCommand(Tmovie movie) : IRequest<List<Tmovie>>;
}
