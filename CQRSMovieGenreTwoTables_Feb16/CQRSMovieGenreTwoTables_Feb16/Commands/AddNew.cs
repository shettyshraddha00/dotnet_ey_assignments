﻿using CQRSMovieGenreTwoTables_Feb16.Models;
using MediatR;

namespace CQRSMovieGenreTwoTables_Feb16.Commands
{
    public record AddNew(Tmovie movie) : IRequest<List<Tmovie>>;
}
