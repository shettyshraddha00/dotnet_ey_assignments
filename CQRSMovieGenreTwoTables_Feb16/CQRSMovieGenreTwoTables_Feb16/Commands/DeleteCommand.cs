﻿using MediatR;

namespace CQRSMovieGenreTwoTables_Feb16.Commands
{
    public record DeleteCommand(int id) : IRequest<string>;

}
