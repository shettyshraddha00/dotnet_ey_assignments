﻿using CQRSMovieGenreTwoTables_Feb16.Models;
using MediatR;

namespace CQRSMovieGenreTwoTables_Feb16.Queries
{
    public record GetMoviesQuery : IRequest<List<Tmovie>>;
   
}
