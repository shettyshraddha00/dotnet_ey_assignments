﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HybridInheritance_feb2
{
    class GradFather
    {
        public void land()
        {
            Console.WriteLine("GradFather's land");
        }

    }

    class Father : GradFather
    {

        public void home()
        {
            Console.WriteLine("Father's home");
        }

        public void Car()
        {
            Console.WriteLine("Father's Car");
        }
    }


    class Son : Father
    {

        public void mobile()
        {
            Console.WriteLine("Son's mobile");
        }
    }

    class Daughter : Father
    {
        public void purse()
        {
            Console.WriteLine("Daughter's purse");
        }
    }
    public class TestHybridInheritance
    {
        public static void Main(String[] args)
        {
            Son s = new Son();
            s.land();// Grand father method
            s.Car(); // Father method
            s.home();// Father method
            s.mobile();// son method
            Daughter d = new Daughter();
            d.land();// Grand father method
            d.Car(); // Father method
            d.home();// Father method
            d.purse();// son method

        }
    }
}
