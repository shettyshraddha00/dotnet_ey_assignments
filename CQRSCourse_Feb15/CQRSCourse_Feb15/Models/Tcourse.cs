﻿using System;
using System.Collections.Generic;

namespace CQRSCourse_Feb15.Models
{
    public partial class Tcourse
    {
        public int CourseId { get; set; }
        public string? CourseName { get; set; }
        public int? CourseHour { get; set; }
    }
}
