﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CQRSCourse_Feb15.Queries;
using CQRSCourse_Feb15.Command;
using CQRSCourse_Feb15.Models;

namespace CQRSCourse_Feb15.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CourseController(IMediator mediator)
        {
            _mediator= mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCourses() 
        {
            var courses= await _mediator.Send(new GetCourseQuery());
            return Ok(courses);
        
        }

        [HttpPost]
        public async Task<IActionResult> AddNewCourse(Tcourse tcourse)
        {
            await _mediator.Send(new AddNew(tcourse));
            return StatusCode(201);

        }
        [HttpPut]
        public async Task<IActionResult> UpdateCourse(Tcourse tcourse)
        {
            await _mediator.Send(new UpdateCommand(tcourse));
            return StatusCode(201);

        }
        [HttpDelete]
        public async Task<IActionResult> DeleteCourse(int id)
        {
            await _mediator.Send(new DeleteCommand(id));
            return StatusCode(200);

        }

    }
}
