﻿using CQRSCourse_Feb15.Models;
using MediatR;

namespace CQRSCourse_Feb15.Command
{
    public record AddNew(Tcourse course): IRequest<List<Tcourse>>;
    
}
