﻿using MediatR;

namespace CQRSCourse_Feb15.Command
{
    public record DeleteCommand(int id): IRequest< string>;
    
}
