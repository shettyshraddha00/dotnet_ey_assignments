﻿using CQRSCourse_Feb15.DataAccess;
using CQRSCourse_Feb15.Models;

namespace CQRSCourse_Feb15.Repositories
{
    public class CourseRepository : ICourse
    {
        private readonly CourseContext _context;

        public CourseRepository(CourseContext context)
        {
            _context = context;
        }

        public List<Tcourse> AddCourse(Tcourse course)
        {
            _context.Tcourses.Add(course);
            _context.SaveChanges();
            return _context.Tcourses.ToList();
        }

        public string deleteCourse(int id)
        {
            var getCourseById = _context.Tcourses.Find(id);
            if(getCourseById != null)
            {
                _context.Tcourses.Remove(getCourseById);
                _context.SaveChanges();
            }
            return "Deleted";

        }

        public List<Tcourse> GetAllCourses()
        {
            return _context.Tcourses.ToList();
        }

        public List<Tcourse> updateCourse(Tcourse course)
        {
            var getCourseById = _context.Tcourses.Find(course.CourseId);
            if (getCourseById != null)
            {
                getCourseById.CourseName = course.CourseName;
                getCourseById.CourseHour= course.CourseHour;
                
            }
           _context.SaveChanges();
            return _context.Tcourses.ToList();
        }
    }
}
