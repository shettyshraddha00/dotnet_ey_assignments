﻿using CQRSCourse_Feb15.Models;
using MediatR;

namespace CQRSCourse_Feb15.Queries
{
    public record GetCourseQuery : IRequest<List<Tcourse>>;
    
}
