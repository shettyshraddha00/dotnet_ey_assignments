﻿using CQRSCourse_Feb15.Models;

namespace CQRSCourse_Feb15.DataAccess
{
    public interface ICourse
    {
        List<Tcourse> GetAllCourses();

        List<Tcourse>  AddCourse(Tcourse course);

        List<Tcourse> updateCourse(Tcourse course);

        string deleteCourse(int id);
    }
}
