﻿using CQRSCourse_Feb15.Command;
using CQRSCourse_Feb15.DataAccess;
using CQRSCourse_Feb15.Models;
using MediatR;

namespace CQRSCourse_Feb15.Handlers
{
    public class AddNewCommandHandler : IRequestHandler<AddNew, List<Tcourse>>
    {
        private readonly ICourse _course;

        public AddNewCommandHandler(ICourse course)
        {
            _course = course;
        }

        public async Task<List<Tcourse>> Handle(AddNew request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_course.AddCourse(request.course));
        }
    }
}
