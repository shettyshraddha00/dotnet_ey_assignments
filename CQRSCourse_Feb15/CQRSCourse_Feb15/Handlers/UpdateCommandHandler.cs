﻿using CQRSCourse_Feb15.Command;
using CQRSCourse_Feb15.DataAccess;
using CQRSCourse_Feb15.Models;
using MediatR;

namespace CQRSCourse_Feb15.Handlers
{
    public class UpdateCommandHandler:IRequestHandler<UpdateCommand,List<Tcourse>>
    {
        private readonly ICourse _course;

        public UpdateCommandHandler(ICourse course)
        {
            _course = course;
        }

        public async Task<List<Tcourse>> Handle(UpdateCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_course.updateCourse(request.course));
        }
    }
}
