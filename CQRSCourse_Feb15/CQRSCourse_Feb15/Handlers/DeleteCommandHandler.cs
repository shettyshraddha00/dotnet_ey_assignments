﻿using CQRSCourse_Feb15.Command;
using CQRSCourse_Feb15.DataAccess;
using MediatR;

namespace CQRSCourse_Feb15.Handlers
{
    public class DeleteCommandHandler : IRequestHandler<DeleteCommand, string>
    {
        private readonly ICourse _course;

        public DeleteCommandHandler(ICourse course)
        {
            _course = course;
        }

        public async  Task<string> Handle(DeleteCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_course.deleteCourse(request.id));
        }
    }
}
