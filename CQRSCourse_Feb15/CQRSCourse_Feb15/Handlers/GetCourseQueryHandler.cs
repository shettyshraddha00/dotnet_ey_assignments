﻿using CQRSCourse_Feb15.DataAccess;
using CQRSCourse_Feb15.Models;
using CQRSCourse_Feb15.Queries;
using MediatR;

namespace CQRSCourse_Feb15.Handlers
{
    public class GetCourseQueryHandler : IRequestHandler<GetCourseQuery, List<Tcourse>>
    {
        private readonly ICourse _course;

        public GetCourseQueryHandler(ICourse course)
        {
            _course = course;
        }

        public async Task<List<Tcourse>> Handle(GetCourseQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_course.GetAllCourses());
        }
    }
}
