﻿using System;
using System.Collections.Generic;

namespace WebApiExample_Feb9.Models
{
    public partial class Tproduct
    {
        public int ProductId { get; set; }
        public string? ProductName { get; set; }
        public string? ProductPrice { get; set; }

        
    }
}
