﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApiExample_Feb9.Models;


namespace WebApiExample_Feb9.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private readonly ProductsContext dbContext;
        public ProductsController(ProductsContext dbContext) {
            this.dbContext=dbContext;
        
        }

        [HttpGet]
        public async Task<IActionResult> GetProducts()
        { 
           
            return Ok(await dbContext.Tproducts.ToListAsync());
        }

        [HttpPost]
        public async Task<IActionResult> AddProducts(AddProduct addProduct)
        {
            var product = new Tproduct()
            {
                ProductId=addProduct.ProductId,
                ProductName=addProduct.ProductName,
                ProductPrice=addProduct.ProductPrice
            };
            await dbContext.Tproducts.AddAsync(product);
            await dbContext.SaveChangesAsync();
            return Ok(product);

        }


    }
}
