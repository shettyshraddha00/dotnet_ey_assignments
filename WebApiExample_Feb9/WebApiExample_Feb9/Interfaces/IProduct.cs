﻿using WebApiExample_Feb9.Models;

namespace WebApiExample_Feb9.Interfaces
{
    public interface IProduct
    {

        // for the easy access od data converting to list 
        List<Tproduct> createNewProduct(IProduct p);
    }
}
