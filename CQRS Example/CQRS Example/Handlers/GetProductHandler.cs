﻿using CQRS_Example.DataAccess;
using CQRS_Example.Models;
using CQRS_Example.Queries;
using MediatR;

namespace CQRS_Example.Handlers
{
    public class GetProductHandler : IRequestHandler<GetProductQuery, List<Tproduct>>
    {
        private readonly IProduct _product;
        public GetProductHandler(IProduct product) => this._product = product;
        
        public async Task<List<Tproduct>> Handle(GetProductQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_product.GetProducts());
        }
    }
}
