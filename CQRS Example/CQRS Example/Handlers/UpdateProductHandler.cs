﻿using CQRS_Example.Commands;
using CQRS_Example.DataAccess;
using CQRS_Example.Models;
using MediatR;

namespace CQRS_Example.Handlers
{
    public class UpdateProductHandler : IRequestHandler<UpdateProductCommand, List<Tproduct>>
    {
        private readonly IProduct _product;
        public UpdateProductHandler(IProduct product)
        {
            _product = product;
        }
        public async Task<List<Tproduct>> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_product.UpdateProduct(request.Tproduct));
        }
    }
}
