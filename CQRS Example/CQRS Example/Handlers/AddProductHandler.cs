﻿using CQRS_Example.Commands;
using CQRS_Example.DataAccess;
using CQRS_Example.Models;
using MediatR;

namespace CQRS_Example.Handlers
{
    public class AddProductHandler : IRequestHandler<AddProductCommand, List<Tproduct>>
    {
        private readonly IProduct _Product;
        public AddProductHandler(IProduct product) 
        {
            _Product = product;
        }
        public async Task<List<Tproduct>> Handle(AddProductCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_Product.CreateNew(request.Tproduct));
        }
    }
}
