﻿using CQRS_Example.Commands;
using CQRS_Example.DataAccess;
using MediatR;

namespace CQRS_Example.Handlers
{
    public class DeleteProductHandler : IRequestHandler<DeleteProductCommand, string>
    {
        private readonly IProduct _product;
        public DeleteProductHandler(IProduct product)
        {
            _product = product;
        }
        public async Task<string> Handle(DeleteProductCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_product.DeleteProduct(request.id));
        }

        
    }
}
