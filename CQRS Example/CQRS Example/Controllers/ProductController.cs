﻿using CQRS_Example.Commands;
using CQRS_Example.Models;
using CQRS_Example.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CQRS_Example.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ISender _mediator;
        public ProductController(ISender mediator)
        {
            _mediator=mediator;
        }
        [HttpGet]
        public async Task<IActionResult> GetProducts() 
        {
            var products= await _mediator.Send(new GetProductQuery());
            return Ok(products);
        }
        [HttpPost]
        public async Task<ActionResult> CreateNew([FromBody] Tproduct tproduct)
        {
            await _mediator.Send(new AddProductCommand(tproduct));
            return StatusCode(201);
        }
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] Tproduct tproduct)
        {
            await _mediator.Send(new UpdateProductCommand(tproduct));
            return StatusCode(201);
        }
        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            await _mediator.Send(new DeleteProductCommand(id));
            return StatusCode(200);
        }

    }
}
