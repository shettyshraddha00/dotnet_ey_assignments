﻿using MediatR;

namespace CQRS_Example.Commands
{
    public record DeleteProductCommand(int id):IRequest<string>;
    
}
