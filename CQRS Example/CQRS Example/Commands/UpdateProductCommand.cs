﻿using CQRS_Example.Models;
using MediatR;

namespace CQRS_Example.Commands
{
    public record UpdateProductCommand(Tproduct Tproduct):IRequest<List<Tproduct>>;
    
}
