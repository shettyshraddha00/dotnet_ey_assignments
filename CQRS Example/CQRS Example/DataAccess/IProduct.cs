﻿using CQRS_Example.Models;
using System.ComponentModel;

namespace CQRS_Example.DataAccess
{
    public interface IProduct
    {
        //get all the product details
        List<Tproduct> GetProducts();
        //insert a new record
        public List<Tproduct> CreateNew(Tproduct tproduct);
        //update a existing record
        public List<Tproduct> UpdateProduct(Tproduct tproduct);
        //delete a record
        public string DeleteProduct(int id);
    }
}
