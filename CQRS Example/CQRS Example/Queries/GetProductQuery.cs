﻿using CQRS_Example.Models;
using MediatR;

namespace CQRS_Example.Queries
{
    public record GetProductQuery:IRequest<List<Tproduct>>;
    
}
