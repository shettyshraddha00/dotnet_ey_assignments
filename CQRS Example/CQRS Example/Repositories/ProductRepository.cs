﻿using CQRS_Example.DataAccess;
using CQRS_Example.Models;

namespace CQRS_Example.Repositories
{
    public class ProductRepository:IProduct
    {
        //implementing datacontext for crud operations
        private readonly ProductsContext _context;
        public ProductRepository(ProductsContext context) 
        {
            _context = context;
        }

        public List<Tproduct> CreateNew(Tproduct tproduct)
        {
            _context.Tproducts.Add(tproduct);
            _context.SaveChanges();
            return _context.Tproducts.ToList(); 
        }

        public string DeleteProduct(int id)
        {
            var getproductid=_context.Tproducts.Find(id);
            if (getproductid != null) 
            {
                _context.Tproducts.Remove(getproductid);
                _context.SaveChanges();
                return "Sucess";
            }
            else
            {
                return "Not Found";
            }          
        }

        public List<Tproduct> GetProducts()
        {
            return _context.Tproducts.ToList();
        }

        public List<Tproduct> UpdateProduct(Tproduct tproduct)
        {
            var getproductid=_context.Tproducts.Find(tproduct.ProductId); 
            if (getproductid != null) 
            {
                getproductid.ProductName = tproduct.ProductName;
                getproductid.ProductPrice = tproduct.ProductPrice;
            }
            _context.SaveChanges();
            return _context.Tproducts.ToList();
        }
    }
}
