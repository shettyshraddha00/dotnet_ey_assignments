﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListCollection_Feb7
{
    internal class Program
    {
        static void Main(string[] args)
        {

            var studentList = new List<Student>();
            {

            studentList.Add(new Student { id=1, name="Shraddha", age=22, course="CSE" });
            studentList.Add(new Student { id=2, name="Arshia", age=23, course="ISE" });
            studentList.Add(new Student { id=3, name="Sanjana", age=22, course="ECE" });
            studentList.Add(new Student { id=4, name="Shetty", age=23, course="ISE" });
            studentList.Add(new Student { id=5, name="Caroline", age=22, course="CSE" });
            studentList.Add(new Student { id=6, name="Aria", age=23, course="CSE" });
            
            foreach( Student stud in studentList)
                {
                    Console.WriteLine(stud.id+ " " + stud.name +" "+ stud.course);
                   
                }
            Console.WriteLine(studentList.Count);
            
           };

        }
    }
    class Student
    {
        public int id;
        public string name;
        public int age;
        public string course;

        //public int studentid { get; set; }

    }
}
