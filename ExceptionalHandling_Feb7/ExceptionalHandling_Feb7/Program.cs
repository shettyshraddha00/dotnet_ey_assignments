﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionalHandling_Feb7
{
    internal class Program : ISavingsAccount, ICurrentAccount
    {
        static void Main(string[] args)
        {
            //#Exceptional Handling

            try
            {
                //functionalities
                var students = new List<string>();
                students.Add("Shraddha");
                students.Add("Shetty");
                students.Add("Arshia");
                students.Add("Aria");

                foreach(var i in students)
                {
                    Console.WriteLine(i + " ");
                }

                var stud = new List<string>() {"Shraddha", "Shetty","Arshia" }; 
                foreach( var j in stud)
                {
                    Console.WriteLine(j + " ");
                }

                for(int i=0;i<students.Count;i++)
                {
                    Console.WriteLine(students[i] + " ");
                }
               
            }
            catch (ArgumentException argex)
            {
                Console.WriteLine(argex.Message);
            }
            catch(IndexOutOfRangeException index)
            {
                Console.WriteLine(index.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Program p = new Program();
            p.checkBalance();
            p.withdrawlAmount();
            p.AddNewAccount();
            p.RemoveAccount();
           
        }

        public void AddNewAccount()
        {
            Console.WriteLine("New Account added:");
        }

        public void checkBalance()
        {
            Console.WriteLine("The balance of the account can be checked here:");
        }

        public void RemoveAccount()
        {
            Console.WriteLine("Account removed:");
        }

        public void withdrawlAmount()
        {
            Console.WriteLine("The amount can be withdrawn here: ");
        }
    }
}
