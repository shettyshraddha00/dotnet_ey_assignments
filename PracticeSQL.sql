--CREATING NEW DATABASE

--create database testd1;

--USE DATABASE
--use testd1;

--CREATING NEW TABLE

--create table tb1
--(
--id int not null,
--frstName varchar(20) not null,
--lastName varchar(10)not null,
--fullAddress varchar(200) null,
--dateOfJoining date null
--);

--INSERTING RECORDS

--insert into tb1 values(1,'Shraddha','Shetty','Udupi','2000-12-12');

--INSERTING ONLY ID, FIRST NAME AND LASTNAME

--insert into tb1(id,frstName,lastName) values(2,'Sanjana','Shetty');

--insert into tb1 values(1,'Shraddha','Shetty','Udupi',' ');

--SELECTION(READ DATA)

--select * from tb1
--select * from t_student2
--select * from t_course

--insert into tb1 values(1,'Shraddha','Shetty','Udupi',NULL);

--insert into tb1 values(4,'Arya','Shetty','Bangalore','2000-09-08');

--UPDATE RECORD
--update tb1 set fullAddress='Kundapura' where id=2;

--update tb1 set frstName='Saman',lastName='Hegde' where id=1 and dateOfJoining='2000-12-12';

--IS NULL
--update tb1 set frstName='Shamana',lastName='Hegde' where id=1 and dateOfJoining is null;

--DELETE
--delete from tb1 where frstName='Shamana';

--TO DELETE EVERYTHING
--delete from tb1 ;

--CREATE A PRIMARY KEY 

--create table t_student2
--(
--studentid int primary key,
--sname varchar(20),
--age int ,
--courseid int not null
--)

--create table t_course
--(
--id int primary key,
--cname varchar(20),
--)


--TO CREATE VIEW
--create view view_frstName as select frstName from tb1;
--create view view_ALLDATA as select * from tb1;

--select * from view_frstName;
--select * from view_ALLDATA;

--TO ADD FOREIGN KEY
--alter table t_student2 add foreign key(courseid) references t_course(id);

--ID INCREMENT BY 1
--	id int identity(1,1)

--CREATE INDEX (FETCHING RECORDS FASTER)

--create index indexname on tablename(column_name)
--create index indexname on tablename(column1,column2)

--create index idx_name on tb1(frstName)

--DROP AN INDEX
--drop index indexname


--INNER JOIN /JOIN(default join-return when combination of two columns are true)
--select * from t_course INNER JOIN t_student2 on t_course.id=t_student2.courseid

--OUTER JOIN
--select * from t_student2 OUTERJOIN t_course on t_course.id=t_student2.courseid

--LEFT OUTER JOIN---> displays the data of the left table with matching data of right table

--select * from t_student2 LEFT OUTER JOIN t_course on t_student2.courseid=t_course.id

--RIGHT OUTER JOIN ---> vice versa of above
--select * from t_student2 RIGHT OUTER JOIN t_course on t_student2.courseid=t_course.id

--CROSS JOIN
--select * from t_student2 CROSS JOIN t_course on t_student2.courseid=t_course.id

--SELF JOIN(HOME WORK)
--select s.studentid,sa.sname from t_student2 s , t_student2 sa where s.studentid<sa.studentid


--THE DATBASE SHUD BE IN SINGLE USER MODE
--PROPERTIES => STATE => RESTRICTED ACCESS => SINGLE USER
--RENAME DATABASE NAME
--USE MASTER
--alter database testd1 modify name=testdb2

--RENAME TABLE NAME
--alter table tb1 rename 

--ADD NEW COLUMN
--alter table tb1 add departmentid int

--DROP COLUMN
--alter table tb1 drop column departmentid


