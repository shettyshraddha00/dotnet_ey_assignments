﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibraryManagementTestMVC_Feb13.Models;

namespace LibraryManagementTestMVC_Feb13.Controllers
{
    public class StudentTestsController : Controller
    {
        private readonly LibraryManagementSystemContext _context;

        public StudentTestsController(LibraryManagementSystemContext context)
        {
            _context = context;
        }

        // GET: StudentTests
        public async Task<IActionResult> Index()
        {
              return View(await _context.StudentTests.ToListAsync());
        }

        // GET: StudentTests/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.StudentTests == null)
            {
                return NotFound();
            }

            var studentTest = await _context.StudentTests
                .FirstOrDefaultAsync(m => m.StudentId == id);
            if (studentTest == null)
            {
                return NotFound();
            }

            return View(studentTest);
        }

        // GET: StudentTests/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: StudentTests/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StudentId,StudentFirstName,StudentLastName,StudentAge,StudentDepartment")] StudentTest studentTest)
        {
            if (ModelState.IsValid)
            {
                _context.Add(studentTest);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(studentTest);
        }

        // GET: StudentTests/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.StudentTests == null)
            {
                return NotFound();
            }

            var studentTest = await _context.StudentTests.FindAsync(id);
            if (studentTest == null)
            {
                return NotFound();
            }
            return View(studentTest);
        }

        // POST: StudentTests/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StudentId,StudentFirstName,StudentLastName,StudentAge,StudentDepartment")] StudentTest studentTest)
        {
            if (id != studentTest.StudentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(studentTest);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentTestExists(studentTest.StudentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(studentTest);
        }

        // GET: StudentTests/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.StudentTests == null)
            {
                return NotFound();
            }

            var studentTest = await _context.StudentTests
                .FirstOrDefaultAsync(m => m.StudentId == id);
            if (studentTest == null)
            {
                return NotFound();
            }

            return View(studentTest);
        }

        // POST: StudentTests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.StudentTests == null)
            {
                return Problem("Entity set 'LibraryManagementSystemContext.StudentTests'  is null.");
            }
            var studentTest = await _context.StudentTests.FindAsync(id);
            if (studentTest != null)
            {
                _context.StudentTests.Remove(studentTest);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StudentTestExists(int id)
        {
          return _context.StudentTests.Any(e => e.StudentId == id);
        }
    }
}
