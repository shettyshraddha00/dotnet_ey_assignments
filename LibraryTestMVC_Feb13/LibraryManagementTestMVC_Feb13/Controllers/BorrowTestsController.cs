﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibraryManagementTestMVC_Feb13.Models;

namespace LibraryManagementTestMVC_Feb13.Controllers
{
    public class BorrowTestsController : Controller
    {
        private readonly LibraryManagementSystemContext _context;

        public BorrowTestsController(LibraryManagementSystemContext context)
        {
            _context = context;
        }

        // GET: BorrowTests
        public async Task<IActionResult> Index()
        {
            var libraryManagementSystemContext = _context.BorrowTests.Include(b => b.Author).Include(b => b.Book).Include(b => b.Student);
            return View(await libraryManagementSystemContext.ToListAsync());
        }

        // GET: BorrowTests/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.BorrowTests == null)
            {
                return NotFound();
            }

            var borrowTest = await _context.BorrowTests
                .Include(b => b.Author)
                .Include(b => b.Book)
                .Include(b => b.Student)
                .FirstOrDefaultAsync(m => m.BorrowId == id);
            if (borrowTest == null)
            {
                return NotFound();
            }

            return View(borrowTest);
        }

        // GET: BorrowTests/Create
        public IActionResult Create()
        {
            ViewData["AuthorId"] = new SelectList(_context.AuthorTests, "AuthorId", "AuthorId");
            ViewData["BookId"] = new SelectList(_context.BookTests, "BookId", "BookId");
            ViewData["StudentId"] = new SelectList(_context.StudentTests, "StudentId", "StudentId");
            return View();
        }

        // POST: BorrowTests/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BorrowId,TakenDate,BookId,AuthorId,StudentId")] BorrowTest borrowTest)
        {
            if (ModelState.IsValid)
            {
                _context.Add(borrowTest);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AuthorId"] = new SelectList(_context.AuthorTests, "AuthorId", "AuthorId", borrowTest.AuthorId);
            ViewData["BookId"] = new SelectList(_context.BookTests, "BookId", "BookId", borrowTest.BookId);
            ViewData["StudentId"] = new SelectList(_context.StudentTests, "StudentId", "StudentId", borrowTest.StudentId);
            return View(borrowTest);
        }

        // GET: BorrowTests/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.BorrowTests == null)
            {
                return NotFound();
            }

            var borrowTest = await _context.BorrowTests.FindAsync(id);
            if (borrowTest == null)
            {
                return NotFound();
            }
            ViewData["AuthorId"] = new SelectList(_context.AuthorTests, "AuthorId", "AuthorId", borrowTest.AuthorId);
            ViewData["BookId"] = new SelectList(_context.BookTests, "BookId", "BookId", borrowTest.BookId);
            ViewData["StudentId"] = new SelectList(_context.StudentTests, "StudentId", "StudentId", borrowTest.StudentId);
            return View(borrowTest);
        }

        // POST: BorrowTests/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BorrowId,TakenDate,BookId,AuthorId,StudentId")] BorrowTest borrowTest)
        {
            if (id != borrowTest.BorrowId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(borrowTest);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BorrowTestExists(borrowTest.BorrowId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AuthorId"] = new SelectList(_context.AuthorTests, "AuthorId", "AuthorId", borrowTest.AuthorId);
            ViewData["BookId"] = new SelectList(_context.BookTests, "BookId", "BookId", borrowTest.BookId);
            ViewData["StudentId"] = new SelectList(_context.StudentTests, "StudentId", "StudentId", borrowTest.StudentId);
            return View(borrowTest);
        }

        // GET: BorrowTests/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.BorrowTests == null)
            {
                return NotFound();
            }

            var borrowTest = await _context.BorrowTests
                .Include(b => b.Author)
                .Include(b => b.Book)
                .Include(b => b.Student)
                .FirstOrDefaultAsync(m => m.BorrowId == id);
            if (borrowTest == null)
            {
                return NotFound();
            }

            return View(borrowTest);
        }

        // POST: BorrowTests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.BorrowTests == null)
            {
                return Problem("Entity set 'LibraryManagementSystemContext.BorrowTests'  is null.");
            }
            var borrowTest = await _context.BorrowTests.FindAsync(id);
            if (borrowTest != null)
            {
                _context.BorrowTests.Remove(borrowTest);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BorrowTestExists(int id)
        {
          return _context.BorrowTests.Any(e => e.BorrowId == id);
        }
    }
}
