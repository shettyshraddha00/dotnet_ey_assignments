﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibraryManagementTestMVC_Feb13.Models;

namespace LibraryManagementTestMVC_Feb13.Controllers
{
    public class BookTestsController : Controller
    {
        private readonly LibraryManagementSystemContext _context;

        public BookTestsController(LibraryManagementSystemContext context)
        {
            _context = context;
        }

        // GET: BookTests
        public async Task<IActionResult> Index()
        {
            var libraryManagementSystemContext = _context.BookTests.Include(b => b.Author);
            return View(await libraryManagementSystemContext.ToListAsync());
        }

        // GET: BookTests/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.BookTests == null)
            {
                return NotFound();
            }

            var bookTest = await _context.BookTests
                .Include(b => b.Author)
                .FirstOrDefaultAsync(m => m.BookId == id);
            if (bookTest == null)
            {
                return NotFound();
            }

            return View(bookTest);
        }

        // GET: BookTests/Create
        public IActionResult Create()
        {
            ViewData["AuthorId"] = new SelectList(_context.AuthorTests, "AuthorId", "AuthorId");
            return View();
        }

        // POST: BookTests/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BookId,BookName,BookPageCount,AuthorId")] BookTest bookTest)
        {
            if (ModelState.IsValid)
            {
                _context.Add(bookTest);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AuthorId"] = new SelectList(_context.AuthorTests, "AuthorId", "AuthorId", bookTest.AuthorId);
            return View(bookTest);
        }

        // GET: BookTests/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.BookTests == null)
            {
                return NotFound();
            }

            var bookTest = await _context.BookTests.FindAsync(id);
            if (bookTest == null)
            {
                return NotFound();
            }
            ViewData["AuthorId"] = new SelectList(_context.AuthorTests, "AuthorId", "AuthorId", bookTest.AuthorId);
            return View(bookTest);
        }

        // POST: BookTests/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BookId,BookName,BookPageCount,AuthorId")] BookTest bookTest)
        {
            if (id != bookTest.BookId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bookTest);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookTestExists(bookTest.BookId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AuthorId"] = new SelectList(_context.AuthorTests, "AuthorId", "AuthorId", bookTest.AuthorId);
            return View(bookTest);
        }

        // GET: BookTests/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.BookTests == null)
            {
                return NotFound();
            }

            var bookTest = await _context.BookTests
                .Include(b => b.Author)
                .FirstOrDefaultAsync(m => m.BookId == id);
            if (bookTest == null)
            {
                return NotFound();
            }

            return View(bookTest);
        }

        // POST: BookTests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.BookTests == null)
            {
                return Problem("Entity set 'LibraryManagementSystemContext.BookTests'  is null.");
            }
            var bookTest = await _context.BookTests.FindAsync(id);
            if (bookTest != null)
            {
                _context.BookTests.Remove(bookTest);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookTestExists(int id)
        {
          return _context.BookTests.Any(e => e.BookId == id);
        }
    }
}
