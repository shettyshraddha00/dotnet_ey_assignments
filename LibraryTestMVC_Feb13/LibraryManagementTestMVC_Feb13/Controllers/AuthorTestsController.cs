﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibraryManagementTestMVC_Feb13.Models;

namespace LibraryManagementTestMVC_Feb13.Controllers
{
    public class AuthorTestsController : Controller
    {
        private readonly LibraryManagementSystemContext _context;

        public AuthorTestsController(LibraryManagementSystemContext context)
        {
            _context = context;
        }

        // GET: AuthorTests
        public async Task<IActionResult> Index()
        {
            var libraryManagementSystemContext = _context.AuthorTests.Include(a => a.Book);
            return View(await libraryManagementSystemContext.ToListAsync());
        }

        // GET: AuthorTests/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.AuthorTests == null)
            {
                return NotFound();
            }

            var authorTest = await _context.AuthorTests
                .Include(a => a.Book)
                .FirstOrDefaultAsync(m => m.AuthorId == id);
            if (authorTest == null)
            {
                return NotFound();
            }

            return View(authorTest);
        }

        // GET: AuthorTests/Create
        public IActionResult Create()
        {
            ViewData["BookId"] = new SelectList(_context.BookTests, "BookId", "BookId");
            return View();
        }

        // POST: AuthorTests/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AuthorId,AuthorName,BookId")] AuthorTest authorTest)
        {
            if (ModelState.IsValid)
            {
                _context.Add(authorTest);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BookId"] = new SelectList(_context.BookTests, "BookId", "BookId", authorTest.BookId);
            return View(authorTest);
        }

        // GET: AuthorTests/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.AuthorTests == null)
            {
                return NotFound();
            }

            var authorTest = await _context.AuthorTests.FindAsync(id);
            if (authorTest == null)
            {
                return NotFound();
            }
            ViewData["BookId"] = new SelectList(_context.BookTests, "BookId", "BookId", authorTest.BookId);
            return View(authorTest);
        }

        // POST: AuthorTests/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AuthorId,AuthorName,BookId")] AuthorTest authorTest)
        {
            if (id != authorTest.AuthorId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(authorTest);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AuthorTestExists(authorTest.AuthorId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BookId"] = new SelectList(_context.BookTests, "BookId", "BookId", authorTest.BookId);
            return View(authorTest);
        }

        // GET: AuthorTests/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.AuthorTests == null)
            {
                return NotFound();
            }

            var authorTest = await _context.AuthorTests
                .Include(a => a.Book)
                .FirstOrDefaultAsync(m => m.AuthorId == id);
            if (authorTest == null)
            {
                return NotFound();
            }

            return View(authorTest);
        }

        // POST: AuthorTests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.AuthorTests == null)
            {
                return Problem("Entity set 'LibraryManagementSystemContext.AuthorTests'  is null.");
            }
            var authorTest = await _context.AuthorTests.FindAsync(id);
            if (authorTest != null)
            {
                _context.AuthorTests.Remove(authorTest);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AuthorTestExists(int id)
        {
          return _context.AuthorTests.Any(e => e.AuthorId == id);
        }
    }
}
