﻿using System;
using System.Collections.Generic;

namespace LibraryManagementTestMVC_Feb13.Models
{
    public partial class StudentTest
    {
        public StudentTest()
        {
            BorrowTests = new HashSet<BorrowTest>();
        }

        public int StudentId { get; set; }
        public string? StudentFirstName { get; set; }
        public string? StudentLastName { get; set; }
        public string? StudentAge { get; set; }
        public string? StudentDepartment { get; set; }

        public virtual ICollection<BorrowTest> BorrowTests { get; set; }
    }
}
