﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LibraryManagementTestMVC_Feb13.Models
{
    public partial class LibraryManagementSystemContext : DbContext
    {
        public LibraryManagementSystemContext()
        {
        }

        public LibraryManagementSystemContext(DbContextOptions<LibraryManagementSystemContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AuthorTest> AuthorTests { get; set; } = null!;
        public virtual DbSet<BookTest> BookTests { get; set; } = null!;
        public virtual DbSet<BorrowTest> BorrowTests { get; set; } = null!;
        public virtual DbSet<StudentTest> StudentTests { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                optionsBuilder.UseSqlServer("server=localhost;trusted_connection=true;database=LibraryManagementSystem");
//            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AuthorTest>(entity =>
            {
                entity.HasKey(e => e.AuthorId);

                entity.ToTable("AuthorTest");

                entity.Property(e => e.AuthorName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Book)
                    .WithMany(p => p.AuthorTests)
                    .HasForeignKey(d => d.BookId)
                    .HasConstraintName("FK__AuthorTes__BookI__412EB0B6");
            });

            modelBuilder.Entity<BookTest>(entity =>
            {
                entity.HasKey(e => e.BookId);

                entity.ToTable("BookTest");

                entity.Property(e => e.BookName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Author)
                    .WithMany(p => p.BookTests)
                    .HasForeignKey(d => d.AuthorId)
                    .HasConstraintName("FK__BookTest__Author__3D5E1FD2");
            });

            modelBuilder.Entity<BorrowTest>(entity =>
            {
                entity.HasKey(e => e.BorrowId);

                entity.ToTable("BorrowTest");

                entity.Property(e => e.TakenDate).HasColumnType("date");

                entity.HasOne(d => d.Author)
                    .WithMany(p => p.BorrowTests)
                    .HasForeignKey(d => d.AuthorId)
                    .HasConstraintName("FK__BorrowTes__Autho__3E52440B");

                entity.HasOne(d => d.Book)
                    .WithMany(p => p.BorrowTests)
                    .HasForeignKey(d => d.BookId)
                    .HasConstraintName("FK__BorrowTes__BookI__3F466844");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.BorrowTests)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK__BorrowTes__Stude__403A8C7D");
            });

            modelBuilder.Entity<StudentTest>(entity =>
            {
                entity.HasKey(e => e.StudentId);

                entity.ToTable("StudentTest");

                entity.Property(e => e.StudentAge)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentDepartment)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentFirstName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentLastName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
