﻿using System;
using System.Collections.Generic;

namespace LibraryManagementTestMVC_Feb13.Models
{
    public partial class BookTest
    {
        public BookTest()
        {
            AuthorTests = new HashSet<AuthorTest>();
            BorrowTests = new HashSet<BorrowTest>();
        }

        public int BookId { get; set; }
        public string? BookName { get; set; }
        public int? BookPageCount { get; set; }
        public int? AuthorId { get; set; }

        public virtual AuthorTest? Author { get; set; }
        public virtual ICollection<AuthorTest> AuthorTests { get; set; }
        public virtual ICollection<BorrowTest> BorrowTests { get; set; }
    }
}
