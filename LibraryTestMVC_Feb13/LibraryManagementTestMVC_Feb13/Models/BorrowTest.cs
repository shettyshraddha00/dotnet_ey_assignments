﻿using System;
using System.Collections.Generic;

namespace LibraryManagementTestMVC_Feb13.Models
{
    public partial class BorrowTest
    {
        public int BorrowId { get; set; }
        public DateTime? TakenDate { get; set; }
        public int? BookId { get; set; }
        public int? AuthorId { get; set; }
        public int? StudentId { get; set; }

        public virtual AuthorTest? Author { get; set; }
        public virtual BookTest? Book { get; set; }
        public virtual StudentTest? Student { get; set; }
    }
}
