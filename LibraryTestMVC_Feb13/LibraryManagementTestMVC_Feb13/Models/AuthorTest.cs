﻿using System;
using System.Collections.Generic;

namespace LibraryManagementTestMVC_Feb13.Models
{
    public partial class AuthorTest
    {
        public AuthorTest()
        {
            BookTests = new HashSet<BookTest>();
            BorrowTests = new HashSet<BorrowTest>();
        }

        public int AuthorId { get; set; }
        public string? AuthorName { get; set; }
        public int? BookId { get; set; }

        public virtual BookTest? Book { get; set; }
        public virtual ICollection<BookTest> BookTests { get; set; }
        public virtual ICollection<BorrowTest> BorrowTests { get; set; }
    }
}
