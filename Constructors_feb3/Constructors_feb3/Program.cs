﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Constructors_feb3
{
    internal class Program
    {
        public String car;
        //Program() {
        //    car = "Baleno";
           
        //}

        Program(int speed, String name)
        {
            Console.WriteLine("Speed of the car :"+speed);
            Console.WriteLine("Name of the car :" + name);
        }
        Program(String model, int wheeler)
        {
            Console.WriteLine("Model of the car :" + model);
            Console.WriteLine("Two or three wheeler  :" + wheeler);

        }

        static void Main(string[] args)
        {
            Program program1=new Program(1200,"Baleno");
            Program program2 = new Program("Baleno",4);

        }
    }
}
