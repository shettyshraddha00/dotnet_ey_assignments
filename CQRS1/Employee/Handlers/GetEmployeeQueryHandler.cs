﻿using Employee.DataAccess;
using Employee.DataAccess.Interfaces;
using Employee.Models;
using Employee.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.QueryHandler
{
    internal class GetEmployeeQueryHandler : IRequestHandler<GetEmployeeQuery, List<TEmployee>>
    {

        private readonly IEmployee _employee;

        public GetEmployeeQueryHandler(IEmployee employee)
        {
            _employee = employee;
        }

        public Task<List<TEmployee>> Handle(GetEmployeeQuery message,CancellationToken cancellationToken)
        {
           return Task.FromResult(_employee.getAllEmployees());
        }
    }
}
