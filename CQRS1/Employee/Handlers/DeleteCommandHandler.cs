﻿using Employee.Commands;
using Employee.DataAccess.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Handlers
{
    internal class DeleteCommandHandler : IRequestHandler<Delete, int>
    {
        private readonly IEmployee _employee;

        public DeleteCommandHandler(IEmployee employee)
        {
            _employee = employee;
        }

        public Task<int> Handle(Delete request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_employee.deleteEmployee(request.EmployeeId));
        }
    }
}
