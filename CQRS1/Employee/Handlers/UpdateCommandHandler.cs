﻿using Employee.Commands;
using Employee.DataAccess;
using Employee.DataAccess.Interfaces;
using Employee.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Handlers
{
    internal class UpdateCommandHandler :IRequestHandler<Update,TEmployee>
    {
        private readonly IEmployee _employee;

        public UpdateCommandHandler(IEmployee employee)
        {
            _employee = employee;
        }

        public Task<TEmployee> Handle(Update request, CancellationToken cancellationToken)
        {

            var employee = new TEmployee();
            employee.EmployeeName=request.EmployeeName;
            employee.EmployeeSalary=request.EmployeeSalary;
            employee.EmployeeId=request.EmployeeAge;
            return Task.FromResult(_employee.updateStudent(employee));
        }
    }
}
