﻿using Employee.Commands;
using Employee.DataAccess.Interfaces;
using Employee.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Handlers
{
    internal class AddNewCommandHandler : IRequestHandler<AddNew, List<TEmployee>>
    {
        private readonly IEmployee _employee;

        public AddNewCommandHandler(IEmployee employee)
        {
            _employee = employee;
        }

        public Task<List<TEmployee>> Handle(AddNew request, CancellationToken cancellationToken)
        {
            var employee=new TEmployee();
            employee.EmployeeName=request.EmployeeName;
            employee.EmployeeSalary=request.EmployeeSalary;
            employee.EmployeeId=request.EmployeeAge;
            return Task.FromResult(_employee.addEmployee(employee));
        }
    }
}
