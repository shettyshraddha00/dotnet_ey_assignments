﻿using Employee.DataAccess;
using Employee.DataAccess.Interfaces;
using Employee.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Repository
{
    public class EmployeeRepository :IEmployee
    {
        private readonly EmployeeContext _employeeContext;

        public EmployeeRepository(EmployeeContext employeeContext)
        {
            _employeeContext = employeeContext;
        }

        //Get all employees
        public List<TEmployee> getAllEmployees()
        {
            return _employeeContext.TEmployees.ToList();
        }

        //Add New Employee

        public List<TEmployee> addEmployee(TEmployee employee)
        {
            _employeeContext.TEmployees.Add(employee);
            _employeeContext.SaveChanges();
            return _employeeContext.TEmployees.ToList();
        }


        //Get Employee By Id
        public TEmployee getEmployeeByid(int id)
        {
            TEmployee employee = _employeeContext.TEmployees.Find(id);
            //_employeeContext.TEmployees.Find(id);
            return employee;
        }

        //Delete employee

        public string deleteEmployee(int id)
        {

            var deleteobj = _employeeContext.TEmployees.Remove(getEmployeeByid(id));
            _employeeContext.SaveChanges();
            return "deleted";
        }

        public List<TEmployee> updateStudent(TEmployee employee)
        {
            var employeeInfo = _employeeContext.TEmployees.Find(employee.EmployeeId);
            employeeInfo.EmployeeName=employee.EmployeeName;
            employeeInfo.EmployeeAge=employee.EmployeeAge;
            employeeInfo.EmployeeSalary=employee.EmployeeSalary;

            _employeeContext.SaveChanges();
            return _employeeContext.TEmployees.ToList();

        }

        int IEmployee.deleteEmployee(int id)
        {
            throw new NotImplementedException();
        }

        TEmployee IEmployee.updateStudent(TEmployee employee)
        {
            throw new NotImplementedException();
        }
    }
}
