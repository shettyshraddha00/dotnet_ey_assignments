﻿using System;
using System.Collections.Generic;

namespace Employee.Models
{
    public partial class TEmployee
    {
        public int EmployeeId { get; set; }
        public string? EmployeeName { get; set; }
        public int? EmployeeSalary { get; set; }
        public int? EmployeeAge { get; set; }
    }
}
