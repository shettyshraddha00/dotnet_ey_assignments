﻿using Employee.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Commands
{
    internal class Update :IRequest<TEmployee>
    {
        public string EmployeeName { get; set; }
        public int EmployeeId { get; set; }
        public int EmployeeSalary { get; set; }
        public int EmployeeAge { get; set; }

    }
}
