﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Commands
{
    internal class Delete :IRequest<int>
    {
       
        public int EmployeeId { get; set; }
        
    }
}
