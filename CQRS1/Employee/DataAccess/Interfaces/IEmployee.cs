﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Employee.Models;

namespace Employee.DataAccess.Interfaces
{
    public interface IEmployee
    {
        List<TEmployee> getAllEmployees();

        List<TEmployee> addEmployee(TEmployee employee);

        int deleteEmployee(int id);

        TEmployee getEmployeeByid(int id);

        TEmployee updateStudent(TEmployee employee);
    }
}
