﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Employee.Models;
using Employee.DataAccess;
using Employee.DataAccess.Interfaces;
using Employee.Repository;
using Employee.Queries;
using Employee.Handlers;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpController : ControllerBase
    {
      private readonly IMediator _mediator;

        public EmpController(IMediator mediator)
        {
            _mediator= mediator;
        }
       
        [HttpGet]
        public async Task<List<TEmployee>> GetAll()
        {
            return await _mediator.Send(new GetEmployeeQuery());

        }
        

    }
}
