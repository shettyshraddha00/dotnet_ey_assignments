﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultipleInheritance_feb2
{
    interface Car
    {
        void Drive();
    }
    interface Bus
    {
        void Drive();
    }
    class Demo : Car, Bus
    {
        //How to implement the Drive() Method inherited from Bus and Car
        void Car.Drive()    //calling method declared in the interface
        {
            Console.WriteLine("Drive Car");
        }
        void Bus.Drive()
        {
            Console.WriteLine("Drive Bus");
        }
        static void Main()
        {
            Car DemoObject1 = new Demo();
            Bus DemoObject2 = new Demo();
            DemoObject1.Drive();
            DemoObject2.Drive();
           
        }
    }
    }
