﻿using CQRSEmployee_Feb15.DataAccess;
using CQRSEmployee_Feb15.Models;

namespace CQRSEmployee_Feb15.Repository
{
    public class EmployeeRepository : IEmployee
    {
        private readonly EmployeeContext _employeeContext;

        public EmployeeRepository(EmployeeContext employeeContext)
        {
            _employeeContext = employeeContext;
        }

        public List<TEmployee> addNew(TEmployee employee)
        {
            _employeeContext.TEmployees.Add(employee);
            _employeeContext.SaveChanges();
            return  _employeeContext.TEmployees.ToList();
        }

        public string delete(int id)
        {
            var getEmployeeByid=_employeeContext.TEmployees.Find(id);

            if(getEmployeeByid != null) 
            {
                _employeeContext.TEmployees.Remove(getEmployeeByid);
               //this is also imp 
               _employeeContext.SaveChanges();
                return "Deleted";
            }
            else
            {
                return "Not found";
            }
            
        }

        public List<TEmployee> getAllEmployees()
        {
           return _employeeContext.TEmployees.ToList();
        }

        public List<TEmployee> update(TEmployee employee)
        {
            var getEmployeeById=_employeeContext.TEmployees.Find(employee.EmployeeId);
            getEmployeeById.EmployeeName= employee.EmployeeName;
            getEmployeeById.EmployeeSalary= employee.EmployeeSalary;
            getEmployeeById.EmployeeAge= employee.EmployeeAge;

            _employeeContext.SaveChanges();
            return _employeeContext.TEmployees.ToList();
           
        }
       
    }
}
