﻿using CQRSEmployee_Feb15.Models;
using System.ComponentModel;

namespace CQRSEmployee_Feb15.DataAccess
{
    public interface IEmployee
    {

        List<TEmployee> getAllEmployees();

        List<TEmployee> addNew(TEmployee employee);

        List<TEmployee> update(TEmployee employee);

        string delete(int id);
    }
}
