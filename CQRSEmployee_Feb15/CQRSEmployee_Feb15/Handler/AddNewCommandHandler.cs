﻿using CQRSEmployee_Feb15.Command;
using CQRSEmployee_Feb15.DataAccess;
using CQRSEmployee_Feb15.Models;
using MediatR;

namespace CQRSEmployee_Feb15.Handler
{
    public class AddNewCommandHandler : IRequestHandler<AddNew, List<TEmployee>>
    {

        private readonly IEmployee _employee;

        public AddNewCommandHandler(IEmployee employee) 
        {
            _employee= employee;
        }

        public  async Task<List<TEmployee>> Handle(AddNew request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_employee.addNew(request.employee));
        }
    }
}
