﻿using CQRSEmployee_Feb15.Command;
using CQRSEmployee_Feb15.DataAccess;
using MediatR;

namespace CQRSEmployee_Feb15.Handler
{
    public class DeleteCommandHandler :IRequestHandler<DeleteCommand,string>
    {
        private readonly IEmployee _employee;

        public DeleteCommandHandler(IEmployee employee)
        {
            _employee = employee;
        }

        public async Task<string> Handle(DeleteCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_employee.delete(request.id));
        }
    }
}
