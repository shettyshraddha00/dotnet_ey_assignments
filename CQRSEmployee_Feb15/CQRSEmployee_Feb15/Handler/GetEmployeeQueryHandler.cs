﻿using CQRSEmployee_Feb15.DataAccess;
using CQRSEmployee_Feb15.Models;
using CQRSEmployee_Feb15.Queries;
using MediatR;

namespace CQRSEmployee_Feb15.Handler
{
    public class GetEmployeeQueryHandler : IRequestHandler<GetEmployeeQuery, List<TEmployee>>
    {
        private readonly IEmployee _employee;
        public GetEmployeeQueryHandler(IEmployee employee) 
        { 
        
            _employee=employee;
        
        }

        public  async Task<List<TEmployee>> Handle(GetEmployeeQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_employee.getAllEmployees());
        }
    }
}
