﻿using CQRSEmployee_Feb15.Command;
using CQRSEmployee_Feb15.DataAccess;
using CQRSEmployee_Feb15.Models;
using MediatR;

namespace CQRSEmployee_Feb15.Handler
{
    public class UpdateCommandHandler : IRequestHandler<UpdateCommand, List<TEmployee>>
    {
        private readonly IEmployee _employee;
        public UpdateCommandHandler(IEmployee employee)
        {
            _employee = employee;
        }
        public async Task<List<TEmployee>> Handle(UpdateCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_employee.update(request.employee));
        }


        
    }
}
