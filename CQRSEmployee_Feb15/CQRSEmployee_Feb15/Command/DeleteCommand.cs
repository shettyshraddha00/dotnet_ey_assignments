﻿using MediatR;

namespace CQRSEmployee_Feb15.Command
{
    public record DeleteCommand(int id) : IRequest<string>;
   
}
