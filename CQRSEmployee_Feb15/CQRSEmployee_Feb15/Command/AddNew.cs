﻿using CQRSEmployee_Feb15.Models;
using MediatR;

namespace CQRSEmployee_Feb15.Command
{
    public record AddNew(TEmployee employee) : IRequest<List<TEmployee>>;
   
}
