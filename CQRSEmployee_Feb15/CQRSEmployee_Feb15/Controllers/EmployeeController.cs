﻿using CQRSEmployee_Feb15.Command;
using CQRSEmployee_Feb15.Models;
using CQRSEmployee_Feb15.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CQRSEmployee_Feb15.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
       private readonly IMediator _mediator;

        public EmployeeController(IMediator mediator)
        {
            _mediator= mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetProducts()
        {
            var employees = await _mediator.Send(new GetEmployeeQuery());
            return Ok(employees);
        }

        [HttpPost]
        public async Task<ActionResult> CreateNew([FromBody] TEmployee employee)
        {
            await _mediator.Send(new AddNew(employee));
            return StatusCode(201);
        }

        [HttpPut]
        public async Task<IActionResult> updateEmployee([FromBody] TEmployee employee)
        {
            await _mediator.Send(new UpdateCommand(employee));
            return StatusCode(201);
        }

        [HttpDelete]
        public async Task<ActionResult> deleteEmployee(int id)
        {
            await _mediator.Send(new DeleteCommand(id));
            return StatusCode(200);
        }

    }
}
