﻿using CQRSEmployee_Feb15.Models;
using MediatR;

namespace CQRSEmployee_Feb15.Queries
{
    public record GetEmployeeQuery :IRequest<List<TEmployee>>;
    
}
