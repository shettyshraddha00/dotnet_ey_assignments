﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingleInheritance_feb2
{
    class Animal
    {

        public void display()
        {
            Console.WriteLine("I am an animal");
        }

    }
    class Dog : Animal
    {
        public void bark()
        {
            Console.WriteLine("Bow Bow");
        }
    }

    class Program
    {

        static void Main(string[] args)
        {

            // object of derived class
            Dog labrador = new Dog();
           
            labrador.display();
            labrador.bark();


        }

    }
}
