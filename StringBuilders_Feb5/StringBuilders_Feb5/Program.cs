﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringBuilders_Feb5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            StringBuilder s = new StringBuilder("Shraddha");
            
            s.Append("Shetty");
            Console.WriteLine( s);
            //insert in the 6th index
            s.Insert(6, "AAA");
            Console.WriteLine("After inserting AAA after 6th index :"+s);
            // after index 5, remove 3 characters
            s.Remove(5, 3);
            Console.WriteLine("After removing from 5th index {"+s);
            //insert new line
            s.AppendLine("AAABB");
            Console.WriteLine("After appending a new line :"+s);
            s.Replace("tty", "TTY");
            Console.WriteLine(s);
            Console.ReadLine();

        }
    }
}
