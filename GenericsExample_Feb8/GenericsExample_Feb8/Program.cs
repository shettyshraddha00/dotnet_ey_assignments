﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsExample_Feb8
{
    internal class Program
    {

        // class which holds the generic functions

        public class ExampleGenerics
        {
            public void add<Integer>(Integer num1 ,Integer num2 )
            {
                int sum =  num1+num2;
                Console.WriteLine($"First Num:{num1}, Second Num:{num2}, Sum: {sum}");
            }
        }

       // Func<string>(string s1, string s2){}


        
        static void Main(string[] args)
        {
            ExampleGenerics example = new ExampleGenerics();
            example.add(10,20);
            example.subtract(20, 10);
        }
    }
}
