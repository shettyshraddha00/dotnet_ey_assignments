﻿using System;
using System.Collections.Generic;

namespace LibraryTest_MVC.Models
{
    public partial class AuthorT
    {
        public AuthorT()
        {
            BookTs = new HashSet<BookT>();
        }

        public int AuthorId { get; set; }
        public string? AuthorName { get; set; }

        public virtual ICollection<BookT> BookTs { get; set; }
    }
}
