﻿using System;
using System.Collections.Generic;

namespace LibraryTest_MVC.Models
{
    public partial class BorrowT
    {
        public int BorrowId { get; set; }
        public DateTime? TakenDate { get; set; }
        public int? StudentId { get; set; }
        public int? BookId { get; set; }

        public virtual BookT? Book { get; set; }
        public virtual StudentT? Student { get; set; }
    }
}
