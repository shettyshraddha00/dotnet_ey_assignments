﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReferenceEqualAndEqual_Feb7
{
    internal class Program
    {
        static void Main(string[] args)
        {

            object n1 = 1;
            object n2 = 1;
            object n3 = 3;

            string str1 = "Shraddha";
            string str2 = "Shraddha";
            string s3 = str2;
            Console.WriteLine(object.Equals(n1,n2));
            Console.WriteLine(object.ReferenceEquals(n1,n2));

            Console.WriteLine(string.Equals(str1, str2));
            Console.WriteLine(string.ReferenceEquals(str1, str2));
            Console.WriteLine(string.ReferenceEquals(s3, str2));

        }
    }
}
