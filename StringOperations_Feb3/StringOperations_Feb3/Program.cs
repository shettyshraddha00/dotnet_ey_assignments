﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringOperations_Feb3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            String str1 = "hello ";
            String str2 = "world";
            String str3 = "";
            Console.WriteLine(str1.Substring(2,3));
            Console.WriteLine(str1.CompareTo(str2));
            Console.WriteLine(str1.Concat(str2));
            Console.WriteLine(str1.Length);
            Console.WriteLine(str1.IndexOf('e'));
            Console.WriteLine(str1.LastIndexOf('e'));  
            Console.WriteLine(str1.Trim());
            Console.WriteLine(str1.ToUpper());
            Console.WriteLine(str2.ToLower());
            Console.WriteLine(str1.Replace(str1,str2));
            Console.WriteLine(str1.Split('e')) ;
            Console.WriteLine(str1.Equals(str2));
            Console.WriteLine(str1.Reverse());
            Console.WriteLine(str1.Remove(1,3));
            Console.WriteLine(str1.Append('o'));
        }
    }
}
