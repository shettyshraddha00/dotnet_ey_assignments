﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SumDelegates
{

    internal class Program
    {

        public delegate int addNum(int x, int y);
        public delegate string concatStringTwo(string s1, string s2);
      
        public int add(int i, int j)
        {
            return i+j;
        }

        public delegate T sumOf<T>(T a, T b);

        public string concatStringTwoNormal(string s1, string s2)
        {
            return String.Concat(s1, s2);
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            addNum obj = new addNum(p.add);
            concatStringTwo obj2=new concatStringTwo(p.concatStringTwoNormal);
            int res = obj(1, 2);
            Console.WriteLine(res);
            string s3 = obj2("Shraddha", "Shetty");
            Console.WriteLine("String concatenation:"+s3);


            sumOf<int> s = new sumOf<int>(p.add);
            s(10, 20);
            Console.WriteLine("Sum :"+s);

        }
    }
}

