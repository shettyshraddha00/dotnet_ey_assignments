﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorOverloading
{
    class Box
    {
       int length;   // Length of a box
       int breadth;  // Breadth of a box
       int height;   // Height of a box

        public int getVolume()
        {
            return length * breadth * height;
        }
        public void setLength(int len)
        {
            length = len;
        }
        public void setBreadth(int bre)
        {
            breadth = bre;
        }
        public void setHeight(int hei)
        {
            height = hei;
        }

        // Overload + operator to add two Box objects.
        public static Box operator +(Box b, Box c)
        {
            Box box = new Box();
            box.length = b.length + c.length;
            box.breadth = b.breadth + c.breadth;
            box.height = b.height + c.height;
            return box;
        }
    }
    class Tester
    {
        static void Main(string[] args)
        {
            Box Box1 = new Box();   // Declare Box1 of type Box
            Box Box2 = new Box();   // Declare Box2 of type Box
           
            int volume = 0;    

            // box 1 specification
            Box1.setLength(6);
            Box1.setBreadth(7);
            Box1.setHeight(5);

            // box 2 specification
            Box2.setLength(12);
            Box2.setBreadth(13);
            Box2.setHeight(10);

            // volume of box 1
            volume = Box1.getVolume();
            Console.WriteLine("Volume of Box1 : {0}", volume);

            // volume of box 2
            volume = Box2.getVolume();
            Console.WriteLine("Volume of Box2 : {0}", volume);
        }
    }
}
